package main

/*
 Copyright (c) 2014 Marcin Krol <mrkafk@gmail.com>.  All rights reserved.

 Permission to use, copy, modify, and distribute this software and its
 documentation for any purpose and without fee is hereby granted,
 provided that the above copyright notice appear in all copies and that
 both that copyright notice and this permission notice appear in
 supporting documentation.

 MARCIN KROL DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS,
 IN NO EVENT SHALL MARCIN KROL BE LIABLE FOR ANY SPECIAL, INDIRECT
 OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION
 WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

 This software is licensed under MIT License:
 http://opensource.org/licenses/mit-license.php
*/

import (
	"bufio"
	"encoding/base64"
	"encoding/gob"
	"encoding/json"
	"flag"
	"fmt"
	"bitbucket.org/kardianos/osext"
	"github.com/garyburd/redigo/redis"
	// added Compress4PyString
	"github.com/mrkafk/dgolzo"
	"github.com/op/go-logging"
	"github.com/vaughan0/go-ini"
	"io"
	stdlog "log"
	"os"
	"path/filepath"
	"reflect"
	"regexp"
	"runtime"
	"runtime/pprof"
	"strconv"
	"strings"
	"time"
	"unicode/utf8"
)

/*



TODO: save_unmatched operator per section (store unmatched lines in key 'unmatched':{'alog':['aafsads34', ..], 'korbka_log':['30 afsd Dec...', ...]} )

TODO: add other meta info to json structure, like hostname where FileScrubber is running, watched file etc

TODO: log warning if %..% was not expanded in redis_cmd

TODO: compile matchconds on reading config file (prevent bad regex syntax later)
TODO: different networked target backends
TODO: pluggable backends?

DONE: allwords, anyword operators - performance, profile
DONE: saving line numbers



*/

type Config struct {
	Global_backoff_time int64
	File_metadata_db    string
	Max_lines_scanned   int64
	Max_lines_batch     int64
	Batch_backoff_sec   int64
	Line_buf_size       int64
	Sections            []ConfSection
	Log_level           string
	Redis_host          string
	Redis_port          int64
	Redis_prio_key      string
	Task_prio           int64
}

type ConfSection struct {
	Watched_file     string
	Backoff_time     int64
	MatchConds       map[string]string
	Redis_cmd        string
	Rotated_file     string
	Section_name     string
	Set_log_hostname string
	Task_prio        int64
}

type FileMeta struct {
	FilePointer      int64
	LineNo           int64
	RotatedOrMain    bool
	BackoffTimeStamp int64
	RecordNum        int64
}

type MetaDB struct {
	LockPID       int
	LockTimeStamp int64
	FileMetaData  *map[string]FileMeta
}

func maybe_exit_err(elem string, err error) {
	if err != nil {
		log.Error("ERROR: %s : %s", elem, err.Error())
		os.Exit(1)
	}
}

func maybe_exit_bool_err(elem string, ok bool) {
	if !ok {
		log.Error("ERROR: %s ", elem)
		os.Exit(1)
	}
}

var match_cond_prefixes = []string{"regex_", "anyword_", "anywords_", "allwords_", "allword_"}

func warn_exit(elem string, status int) {
	log.Warning("WARNING: %s", elem)
	os.Exit(status)
}

func error_exit(elem string, status int) {
	log.Error("ERROR: %s", elem)
	os.Exit(status)
}

func prefix_in_slice(s string, slist []string) bool {
	for _, b := range slist {
		if strings.HasPrefix(s, b) {
			return true
		}
	}
	return false
}

// ##################### Config #####################

func proc_conf_section(name string, sec ini.Section, file ini.File, parent Config) ConfSection {
	settings := map[string]string{
		"watched_file":     "string",
		"backoff_time":     "int64",
		"redis_cmd":        "string",
		"rotated_file":     "string",
		"set_log_hostname": "string",
		"task_prio":        "int64"}
	var c ConfSection
	c.Backoff_time = parent.Global_backoff_time
	c.Rotated_file = ""
	c.Watched_file = ""
	c.Redis_cmd = ""
	c.Section_name = name
	c.Set_log_hostname = ""
	c.Task_prio = 100
	c.MatchConds = make(map[string]string)
	for s_name, s_type := range settings {
		v, ok := file.Get(name, s_name)
		maybe_exit_bool_err(fmt.Sprintf("Setting \"%s\" missing in section \"%s\" of config file", s_name, name), ok)
		fld_name := strings.Title(s_name)
		v = strings.Trim(v, " \t\n")
		if s_type == "string" {
			reflect.ValueOf(&c).Elem().FieldByName(fld_name).SetString(v)
			continue
		}
		if s_type == "int64" {
			i, err := strconv.ParseInt(v, 10, 64)
			maybe_exit_err("Configuration file", err)
			reflect.ValueOf(&c).Elem().FieldByName(fld_name).SetInt(int64(i))
			continue
		}
		panic("unknown type of configuration variable")
	}
	for k, v := range file[name] {
		if !prefix_in_slice(k, match_cond_prefixes) {
			continue
		}
		c.MatchConds[k] = v
	}
	return c
}

func read_remaining_sections(file ini.File, conf Config) []ConfSection {
	s := make([]ConfSection, 0)
	for name, section := range file {
		if name == "global" {
			continue
		}
		log.Debug("Config section name: %s", name)
		log.Debug("Section content: %s", section)
		s = append(s, proc_conf_section(name, section, file, conf))
	}
	return s
}

func configure_log(level string) {
	eff_level := logging.INFO
	if strings.ToUpper(level) == "DEBUG" {
		eff_level = logging.DEBUG
	}
	logging.SetLevel(eff_level, "")
	var format = logging.MustStringFormatter("%{level:.1s} | %{message}")
	logging.SetFormatter(format)
	logBackend := logging.NewLogBackend(os.Stderr, "", stdlog.LstdFlags|stdlog.Lshortfile)
	logBackend.Color = false
	logging.SetBackend(logBackend)
}

func find_config_dir(fname string) string {
	execpath, _ := osext.Executable()
	execdir := filepath.Dir(execpath)
	dirs_to_search := []string{"/etc", execdir}
	for _, d := range dirs_to_search {
		fpath := filepath.Join(d, fname)
		_, err := os.Open(fpath)
		if err == nil {
			return fpath
		}

	}
	error_exit(fmt.Sprintf("Configuration file (%s) does not exist in any of the following directories: %s", fname, dirs_to_search), 2)
	return ""
}

func read_config(fname string) Config {
	confpath := find_config_dir(fname)
	file, err := ini.LoadFile(confpath)
	maybe_exit_err("Problem opening configuration file", err)
	var conf Config
	conf.Line_buf_size = int64(1048576)
	conf.Max_lines_scanned = int64(-1)
	conf.Max_lines_batch = int64(1000000)
	global_settings := map[string]string{
		"file_metadata_db":    "string",
		"max_lines_scanned":   "int64",
		"max_lines_batch":     "int64",
		"batch_backoff_sec":   "int64",
		"line_buf_size":       "int64",
		"global_backoff_time": "int64",
		"log_level":           "string",
		"redis_host":          "string",
		"redis_port":          "int64",
		"redis_prio_key":      "string"}
	for s_name, s_type := range global_settings {
		val, ok := file.Get("global", s_name)
		if !ok {
			msg := fmt.Sprintf("global / %s setting missing in config file", s_name)
			panic(msg)
		}
		fld_name := strings.Title(s_name)
		val = strings.Trim(val, " \t\n")
		if s_type == "string" {
			reflect.ValueOf(&conf).Elem().FieldByName(fld_name).SetString(val)
			continue
		}
		if s_type == "int64" {
			i, err := strconv.ParseInt(val, 10, 64)
			maybe_exit_err("Configuration file", err)
			log.Debug("Setting fld_name %s to value %d", fld_name, int64(i))
			reflect.ValueOf(&conf).Elem().FieldByName(fld_name).SetInt(int64(i))
			continue
		}
		panic("unknown type of configuration variable")
	}
	configure_log(conf.Log_level)
	conf.Sections = read_remaining_sections(file, conf)
	return conf
}

// ####################### Metadata #######################

func init_metadata(conf Config) MetaDB {
	var mdb MetaDB
	mdb.LockPID = 0
	fmd := make(map[string]FileMeta)
	mdb.FileMetaData = &fmd
	now_ts := time.Now().Unix()
	for _, sec := range conf.Sections {
		var fm FileMeta
		fm.BackoffTimeStamp = now_ts + sec.Backoff_time
		fm.FilePointer = 0
		fm.LineNo = 0
		fm.RotatedOrMain = false
		fmd[sec.Watched_file] = fm
	}
	return mdb
}

// ####################### GOB support (current data) #######################

func save_metadata_db(fpath string, mdb *MetaDB) {
	flag := os.O_CREATE | os.O_RDWR | os.O_TRUNC
	out, err := os.OpenFile(fpath, flag, 0600)
	defer out.Close()
	maybe_exit_err("File metadata db open", err)
	enc := gob.NewEncoder(out)
	enc_err := enc.Encode(mdb)
	maybe_exit_err("Encoding error", enc_err)
}

func read_metadata_db(conf Config) *MetaDB {
	infile, err := os.Open(conf.File_metadata_db)
	if os.IsNotExist(err) {
		bmdb := init_metadata(conf)
		save_metadata_db(conf.File_metadata_db, &bmdb)
		log.Debug("INIT metadata db: %s", conf.File_metadata_db)
		return &bmdb
	}
	defer infile.Close()
	maybe_exit_err("File metadata db open", err)
	var mdb MetaDB
	dec := gob.NewDecoder(infile)
	dec_err := dec.Decode(&mdb)
	maybe_exit_err("Decoding error", dec_err)
	return &mdb
}

// ####################### Locking #######################

func opt_correct_stale_lock(mdb *MetaDB, conf Config) *MetaDB {
	if (*mdb).LockPID != 0 {
		now_ts := time.Now().Unix()
		if (*mdb).LockTimeStamp+conf.Global_backoff_time < now_ts {
			// we have a stale lock - last run instance crashed, were killed, etc
			log.Info("Stale lock (PID: %d, set at: %s, Unix timestamp: %d), removing", mdb.LockPID, time.Unix(mdb.LockTimeStamp, 0), mdb.LockTimeStamp)
			(*mdb).LockPID = 0
			(*mdb).LockTimeStamp = now_ts
		}
	}
	return mdb
}

func set_lock(conf Config, mdb *MetaDB) {
	mdb = opt_correct_stale_lock(mdb, conf)
	mdb_path := conf.File_metadata_db
	now_ts := time.Now().Unix()
	if mdb.LockPID == 0 {
		PID := os.Getpid()
		(*mdb).LockPID = PID
		(*mdb).LockTimeStamp = now_ts
		save_metadata_db(mdb_path, mdb)
		time.Sleep(200 * time.Millisecond)
		mdb = read_metadata_db(conf)
		if mdb.LockPID != PID {
			// Another FileScrubber instance overwrote lock in a split second after us, we're aborting
			warn_exit(fmt.Sprintf("Another instance (PID: %d) overwrote the lock. Our PID: %d. Abort.", mdb.LockPID, PID), 2)
		}
	}
}

func release_lock(conf Config, mdb *MetaDB) {
	mdb.LockPID = 0
	mdb.LockTimeStamp = 0
	save_metadata_db(conf.File_metadata_db, mdb)
}

// JSON
func make_basic_json_structure(mdb *MetaDB) map[string]interface{} {
	json_map := make(map[string]interface{})
	return json_map
}

// #######################  Gather file line matches #######################

func setDefaultMapStringInterface(m map[string]interface{}, k string, v interface{}) (set bool, rv interface{}) {
	if rv, set = m[k]; !set {
		m[k] = v
		rv = v
		set = true
	}
	return
}

func init_match_lists(matchconds map[string]string, sec ConfSection) map[string][]string {
	maplist := make(map[string][]string)
	for k, _ := range matchconds {
		maplist[k] = []string{}
	}
	return maplist
}

func debug_save_matches(matches []string) {
	f, _ := os.OpenFile("/tmp/matches.txt", os.O_CREATE|os.O_RDWR|os.O_APPEND, 0660)
	defer f.Close()
	for _, line := range matches {
		f.Write([]byte(line))
	}
}

func copy_matches2json_struct(match_lists *map[string][]string, json_map_ptr *map[string]interface{}) int64 {
	total := int64(0)
	json_map := *json_map_ptr
	for r_name, match_lst := range *match_lists {
		//log.Debug("JSON key: %s match_lst: %s", r_name, fmt.Sprintf("%.60s ...", fmt.Sprintf("%s", match_lst)))
		json_map[r_name] = match_lst
		total = total + int64(len(match_lst))
		//debug_save_matches(match_lst)
	}
	return total
}

func compile_regexes(reg_map map[string]string) map[string]*regexp.Regexp {
	defer func() {
		r := recover()
		if r != nil {
			log.Error("Following regex could not be compiled: %s", r)
			os.Exit(3)
		}
	}()
	regexes := make(map[string]*regexp.Regexp)
	for k, v := range reg_map {
		if !strings.HasPrefix(k, "regex_") {
			continue
		}
		compiled := regexp.MustCompile(v)
		regexes[k] = compiled
	}
	return regexes
}

func nonempty_slice(sl []string) (out []string) {
	for _, s := range sl {
		out = append(out, strings.Trim(s, " \t\n"))
	}
	return
}

func select_line_ops(prefixes []string, mconds map[string]string) (lineops map[string][]string) {
	lineops = make(map[string][]string)
	for mcond_name, mcond_vals := range mconds {
		for _, pref := range prefixes {
			if strings.HasPrefix(mcond_name, pref) {
				nonempty_words := nonempty_slice(strings.Split(mcond_vals, ", "))
				lineops[mcond_name] = nonempty_words
				log.Debug("mcond_name %s content (nonempty_words): %s", mcond_name, nonempty_words)
			}
		}
	}
	return
}

func replaceInvalidUTFChars(s string) string {
	if !utf8.ValidString(s) {
		n := make([]rune, 0, len(s))
		for _, r := range s {
			if r == utf8.RuneError {
				n = append(n, '?')
			} else {
				n = append(n, r)
			}
		}
		return string(n)
	}
	return s
}

func get_file_len(file *os.File) int64 {
	st, err := file.Stat()
	maybe_exit_err(fmt.Sprintf("Getting stat() on file %s: %s", file, err), err)
	return st.Size()
}

func add_section_matches(file *os.File, fname string, sec ConfSection, conf Config, mdb *MetaDB) (jmap *map[string]interface{}, matches_copied int64, EOFreached bool) {
	EOFreached = false
	fmd_map := mdb.FileMetaData
	fmd := (*fmd_map)[fname]
	fp := fmd.FilePointer
	flen := get_file_len(file)
	if fp > flen {
		log.Info("Last saved file pointer position is greater than current file length, resetting file ptr position to 0")
		fmd.FilePointer = 0
		fp = 0
		fmd.LineNo = 0
	}
	if fp == flen {
		log.Info("No new lines found in file %s", fname)
		EOFreached = true
		return nil, 0, EOFreached
	}
	file.Seek(fp, 0)
	log.Debug("Setting FP to %d", fp)
	reader := bufio.NewReaderSize(file, int(conf.Line_buf_size))
	max_lines := conf.Max_lines_batch
	line_no := int64(0)
	regexes := compile_regexes(sec.MatchConds)
	allwords := select_line_ops([]string{"allwords_", "allword_"}, sec.MatchConds)
	anywords := select_line_ops([]string{"anywords_", "anyword_"}, sec.MatchConds)
	match_lists := init_match_lists(sec.MatchConds, sec)
	bscanned_num := int64(0)
	// scanner.Scan() seems to use io.Reader that reads lines from file in batches, so
	// we can't use current file's FP (file.Seek(0, 1)) to see where it stopped scanning
	for {
		line, err := reader.ReadString('\n')
		if err == io.EOF {
			log.Debug("EOF encountered (line no: %d)", fmd.LineNo+line_no)
			line_len := int64(len(line))
			file.Seek(line_len, -2)
			bscanned_num -= line_len
			EOFreached = true
			break
		}
		if line_no == max_lines {
			break
		}
		line_no++
		text := string(line)
		bscanned_num += int64(len(text))
		//log.Debug("Line (line_no: %d): %s", line_no, text)
		//log.Debug("bscanned_num %d", bscanned_num)

		// All words present in a line
		for allw_name, words := range allwords {
			allpresent := true
			for _, w := range words {
				if !strings.Contains(text, w) {
					allpresent = false
					break
				}
			}
			if allpresent {
				sanitized_text := replaceInvalidUTFChars(text)
				match_lists[allw_name] = append(match_lists[allw_name], sanitized_text)
			}
		}

		// Any word present in a line
		for anyw_name, words := range anywords {
			anypresent := false
			for _, w := range words {
				if strings.Contains(text, w) {
					anypresent = true
					break
				}
			}
			if anypresent {
				sanitized_text := replaceInvalidUTFChars(text)
				match_lists[anyw_name] = append(match_lists[anyw_name], sanitized_text)
			}
		}

		// Regexes
		for r_name, r_compiled := range regexes {
			matches := r_compiled.FindAllString(text, -1)
			if matches != nil {

				for _, m := range matches {
					if m != "" {
						sanitized_text := replaceInvalidUTFChars(m)
						//log.Debug("S4 sanitized_text %s", sanitized_text)
						match_lists[r_name] = append(match_lists[r_name], sanitized_text)
					}
				}
			}
		}
		// End handling Regexes
	}
	json_map := make_basic_json_structure(mdb)
	matches_copied = copy_matches2json_struct(&match_lists, &json_map)
	fmd.FilePointer = fp + bscanned_num
	//log.Debug("NEW FP %d", fmd.FilePointer)
	fmd.LineNo = fmd.LineNo + line_no
	log.Info("Processed section %s (watched file: %s)", sec.Section_name, fname)
	log.Info("Finished reading at line %d Matches copied: %d (lines read: %d)", fmd.LineNo, matches_copied, line_no)
	(*fmd_map)[fname] = fmd
	//log.Debug("match_lists len %d", len(match_lists))
	return &json_map, matches_copied, EOFreached
}

func prep_payload(b *[]byte) *string {
	//var outbuf bytes.Buffer
	//w := zlib.NewWriter(&outbuf)
	//w.Write(*b)
	//w.Close()
	//result := outbuf.Bytes()	log.Debug("FPOS %d", fpos)

	lzocomp, _ := lzo.NewCompressor(lzo.BestCompression)
	compressed, errc := lzocomp.Compress4PyString(*b)
	maybe_exit_err("LZO compression problem", errc)
	s := base64.StdEncoding.EncodeToString(compressed)
	return &s
}

func check_operqueue(s string) bool {
	if s == "%operator%" || s == "%operqueue%" || s == "%oq%" || s == "%operatorqueue%" {
		return true
	}
	return false
}

func check_operqueue_in_string(s string) bool {
	for _, w := range strings.Split(s, " ") {
		if check_operqueue(w) {
			return true
		}
	}
	return false
}

func create_redis_cmd(payload *string, sec ConfSection, conf Config, qname string) []string {
	cmds := sec.Redis_cmd
	redis_args := make([]string, 0)
	for _, elem := range strings.Split(cmds, " ") {
		if elem == "" {
			continue
		}
		if elem == "%section%" {
			redis_args = append(redis_args, sec.Section_name)
			continue
		}
		if elem == "%matches%" {
			redis_args = append(redis_args, *payload)
			continue
		}

		if check_operqueue(elem) {
			redis_args = append(redis_args, qname)
			continue
		}
		if elem == "%timestamp%" {
			redis_args = append(redis_args, fmt.Sprintf("%d", time.Now().Unix()))
			continue
		}
		redis_args = append(redis_args, elem)
	}
	return redis_args
}

func sliceString2SliceInterface(ss []string) (si []interface{}) {
	si = make([]interface{}, len(ss))
	for i, val := range ss {
		si[i] = val
	}
	return
}

func dial_redis(rcmd []string, conf Config) (interface{}, error) {
	redis_addr := fmt.Sprintf("%s:%d", conf.Redis_host, conf.Redis_port)
	conn, err := redis.Dial("tcp", redis_addr)
	defer func() {
		if conn != nil {
			conn.Close()
		}
	}()
	maybe_exit_err(fmt.Sprintf("Could not connect to Redis (%s)", redis_addr), err)
	args_si := sliceString2SliceInterface(rcmd[1:])
	log.Debug("Redis cmd: %s %s", rcmd[0], fmt.Sprintf("%.70s ... ", args_si))
	resp, err := conn.Do(rcmd[0], args_si...)
	log.Debug("Redis reply: %s", fmt.Sprint(resp))
	maybe_exit_err("Redis command error", err)
	return resp, err
}

func redis_cmd(b *[]byte, sec ConfSection, conf Config, qname string) {
	payload := prep_payload(b)
	log.Debug("Payload size %d", len(*payload))
	rcmd := create_redis_cmd(payload, sec, conf, qname)
	dial_redis(rcmd, conf)
}

func incr_record_num(mdb *MetaDB, sec ConfSection) (record_num int64) {
	mdb_section := (*mdb.FileMetaData)[sec.Watched_file]
	mdb_section.RecordNum++
	record_num = mdb_section.RecordNum
	(*mdb.FileMetaData)[sec.Watched_file] = mdb_section
	return
}

func proc_section(file *os.File, fname string, sec ConfSection, conf Config, mdb *MetaDB) (num_copied int64, EOFreached bool) {
	json_map, num_copied, EOFreached := add_section_matches(file, fname, sec, conf, mdb)
	if num_copied == 0 {
		log.Debug("Zero new matches for section %s, no record_num increment and not sending results", sec.Section_name)
		return
	}
	record_num := incr_record_num(mdb, sec)
	(*json_map)["record_num"] = record_num
	log.Info("Section task prio %d", sec.Task_prio)
	if check_operqueue_in_string(sec.Redis_cmd) {
		for oq, v := range *json_map {
			if oq == "record_num" {
				continue
			}
			oqdict := make(map[string]interface{})
			oqdict["record_num"] = record_num
			oqdict["matches"] = v
			oqdict["watched_file"] = sec.Watched_file
			oqdict["log_hostname"] = "UNKNOWN"
			if sec.Set_log_hostname != "" {
				oqdict["log_hostname"] = sec.Set_log_hostname
			} else {
				oqdict["log_hostname"], _ = os.Hostname()
			}
			json_bytes, err := json.Marshal(oqdict)
			maybe_exit_err("JSON marshalling error", err)
			redis_cmd(&json_bytes, sec, conf, oq)
		}
		json_map = nil
		runtime.GC()
	} else {
		json_bytes, err := json.Marshal(json_map)
		//specfo, _ := os.OpenFile("/tmp/redis.json", os.O_RDWR|os.O_TRUNC|os.O_CREATE, 0664)
		//specfo.Write(json_bytes)
		//specfo.Close()
		json_map = nil
		runtime.GC()
		maybe_exit_err("JSON marshalling error", err)
		b := &json_bytes
		redis_cmd(b, sec, conf, "")
	}
	return num_copied, EOFreached
}

func get_watched_file_matches(conf Config, mdb *MetaDB) (total_copied int64) {
	total_copied = int64(0)
	for _, sec := range conf.Sections {
		log.Debug("Section %s", sec)
		if sec.Task_prio < conf.Task_prio {
			log.Info("Current section (%s) priority %d is lower than current Celery priority (%d), section skipped", sec.Section_name, sec.Task_prio, conf.Task_prio)
			continue
		}
		fname := sec.Watched_file
		file, err := os.Open(fname)
		if os.IsNotExist(err) {
			log.Warning("Watched file \"%s\" configured for section \"%s\" does not exist.", fname, sec.Section_name)
			continue
		}

		for {
			num_copied, EOFreached := proc_section(file, fname, sec, conf, mdb)
			//log.Debug("### num_copied: %d EOFreached: %b", num_copied, EOFreached)
			total_copied += num_copied
			if EOFreached {
				break
			}
			// TODO: BUG does not count num of lines scanned correctly, checks for num of matches.
			mls := conf.Max_lines_scanned
			if mls > -1 && total_copied > mls {
				break
			}
			log.Debug("Total copied: %d Max_lines_scanned: %d. Processing section %s again.", total_copied, mls, sec.Section_name)
		}

	}
	return
}


var log = logging.MustGetLogger("")

var cpuprofile = flag.String("cpuprofile", "", "write cpu profile to file")

func get_task_prio(conf Config) int64 {
	redis_prio_key := conf.Redis_prio_key
	cmd := []string{"GET", redis_prio_key}
	resp, err := dial_redis(cmd, conf)
	s, value_err := redis.String(resp, err)
	maybe_exit_err("Error converting Redis response", value_err)
	prio, err := strconv.ParseFloat(s, 64)
	maybe_exit_err("Error converting redis response", err)
	log.Info("task_prio %f", prio)
	return int64(prio)
}

func main() {

	flag.Parse()
	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}

	conf := read_config("filescrubber.ini")
	mdb := read_metadata_db(conf)

	prio := get_task_prio(conf)
	conf.Task_prio = prio
	set_lock(conf, mdb)
	total_copied := get_watched_file_matches(conf, mdb)

	log.Debug("total_copied %d", total_copied)
	release_lock(conf, mdb)
}
