# FileScrubber #

Software Purpose

FileScrubber will be able to efficiently gather data from a log file and forward matched lines (all of them or those matching regular expressions) to Redis. Subsequent runs of this program have to start where the previous run left off (i.e. it has to track file pointer on a log file).

Implemented in Golang for high performance and minimum footprint.


# Requirements #


1. cmdline program


2. Remember file pointer from position of last read.


3. Use locking when reading data to ensure that another FileScrubber instance does not forward the same data or operate on the same file.

### Algorithm ###


Removing stale lock:

If program crashed or were killed, lock may remain there without being removed. To remove stale lock, overwrite it with this instance's PID if time.now() is bigger than backoff timestamp. Proceed.

a. Read if not locked by somebody else (else abort)

b. Set lock with value of PID

c. Wait 100ms. Read lock value again to see if the lock were not overwritten by another FS instance (in a split microsecond before another instance read free lock, this instance wrote lock and instance #2 assumed lock was free and overwrote 1st instance's lock nearly simultaneously -- it's highly improbable but not impossible). If so, abort.

d. set backoff time in future for particular file to db.


## 4. Main operation ##

a. read lines one by one to reduce memory usage.

b. match line against 1..n regexes specified in config. 2nd mode: forward every line.

c. Pack every match into json structure:


```
#!go

{ record_num: n
  regex_a: ["matcha1", "matcha2", ..]
  regex_b: ["matchb1", "matchb2", ..]
  regex_c: ["matchc1", "matchc2", ..]
}

```

record_num is a run number over multiple runs (kept in temp db). Run number in this case is also a number of Redis op (record here is understood as target Redis record, not file line match number). It's reset to 0 on current FP being bigger than file size (meaning the file was truncated or logrotated).

d. compress, encode using base64

e. enable sending by using specified Redis cmd, like:


```
#!go

LPUSH %section% %match%
```



f. if watched file's pointer happens to be beyond the file size, it means that the file was truncated, log rotated, etc. In such case set FP to zero and proceed.

g. Optionally, a rotated file name may be provided to work on the remainder of the log file that was not processed before. In such case set FP on the rotated log file to the last known position before rotation and process until the file end.


## 5. Implementation ##

a. file metadata kept in GOB (go serialization format)


6. Config format


```
#!ini

file_metadata_db = /var/run/filescrubber # '.kch extension is added automatically
max_item_size = 100000000 # maximum size of 1 item to put in Redis in Bytes (before compression and base64)
global_backoff_time = 1 # file backoff time can not be less than this, if not set in section, then this value is selected AND effective backoff time == max(global_backoff_time, section backoff_time)

[section_name]

watched_file = path
backoff_time = 1 # do not proces if less than specified number of seconds passed since last operation on this file
max_batch_lines = 1000000 # maximum number of lines processed in one run - prevent processing burst overloadin machine on
# 1st attempt to process a huge log file. Further line batches will be processed in subsequent runs.

regex = ...
regex = ...
regex = ...
redis_cmd = LPUSH a_queue %match% # pack matched lines into JSON structure and push to Redis
rotated_file = path # if log rotation were detected, process remainder of this file instead


```